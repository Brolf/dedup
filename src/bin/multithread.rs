use std::sync::mpsc;
use std::thread;
use std::time;



fn worker_thread(input: mpsc::Receiver<i32>, output: mpsc::Sender<i32>){

    loop {
        match input.recv() {
            Ok(data) => {
                //thread::sleep(time::Duration::from_millis(data as u64));
                output.send(data).unwrap();
            }
            Err(_) => {
                break;
            }
        }
    }
}

const THREAD_MAX : usize = 5;

fn main() {
    let (return_tx, return_rx) = mpsc::channel();

    let mut channels_rx = Vec::<mpsc::Sender<i32>>::new();

    for _ in 0..THREAD_MAX {
        let (tx, rx) = mpsc::channel();
        channels_rx.push(tx);
        let rtx = return_tx.clone();
        thread::spawn(move || worker_thread(rx, rtx));
    }

    for i in 0..THREAD_MAX {
        channels_rx[i].send((i+1) as i32).unwrap();
    }

    thread::sleep(time::Duration::from_millis(100));

    for i in 0..THREAD_MAX {
        channels_rx[i].send((i*2+1) as i32).unwrap();
    }

    drop(return_tx);
    drop(channels_rx);

    while let Ok(msg) = return_rx.recv() {
        println!("{msg:?}");
    }
}
