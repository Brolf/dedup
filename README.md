# backrub
## After watching https://www.youtube.com/watch?v=w3WYdYyjek4 I have decided to change the scope of this project. I will try to use TigerStyle and develop a distributed backup solution. Furthermore I'll have a deeper look into zig because of the promising std library. 


A Rust project to eventually create a backup tool.
The main repository for this project is https://codeberg.org/Brolf/backrub

My goal with this project is to learn Rust. 
Another aim of this project is that _backrub_ is able to build against musl.


